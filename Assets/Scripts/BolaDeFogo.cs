﻿using UnityEngine;
using System.Collections;

public class BolaDeFogo : MonoBehaviour {

	private float posicaoY = 0f;
	public float altura;
	public float tempo;


	// Use this for initialization
	void Start () {
		Destroy (gameObject,tempo);
		rigidbody2D.AddForce (transform.up * altura);
		posicaoY = transform.position.y;
	
	}
	
	// Update is called once per frame
	void Update () {
		if (posicaoY > transform.position.y) { //Descendo
						transform.eulerAngles = new Vector2 (180, 0);
				}
		posicaoY = transform.position.y;
	
	}

	void OnCollisionEnter2D(Collision2D colisor) { 
		if (colisor.gameObject.tag == "Player") { 
			var player = colisor.gameObject.GetComponent<Player>(); 
			player.PerdeVida(20); 
		} 
		Destroy (gameObject); 
	}
	


}
