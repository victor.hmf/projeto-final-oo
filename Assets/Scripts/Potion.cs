﻿using UnityEngine;
using System.Collections;

public class Potion : MonoBehaviour {

	public int vida;

	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D(Collider2D colisor) { 
		if (colisor.gameObject.tag == "Player") { 
			gameObject.audio.Play(); 
			var player = colisor.gameObject.GetComponent <Player>(); 
			player.RecuperaVida(vida); 
			Destroy(gameObject); 


		} 
	}
		

}
