﻿using UnityEngine;
using System.Collections;

public class PlataformaDestroi : MonoBehaviour {

	private bool pisou;

	void OnCollisionEnter2D(Collision2D colisor) {
		if (colisor.gameObject.name == "Player") {
			pisou = true;
		}
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
				if (pisou) {
						gameObject.AddComponent ("Rigidbody2D");
						Destroy (gameObject, 0f);
				}

	}
}
