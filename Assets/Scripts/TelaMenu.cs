﻿using UnityEngine;
using System.Collections;

public class TelaMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Screen.showCursor = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI () {

		GUI.Box (new Rect (Screen.width / 2 - 145, Screen.height / 2 -70, 300, 100), "");

		if (GUI.Button (new Rect(Screen.width / 2 - 45, Screen.height / 2 -40, 100, 30), "New Game")) {
			Application.LoadLevel("Fase 1");
				}

		if (GUI.Button (new Rect(Screen.width / 2 - 45, Screen.height / 2 -1, 100, 30), "Load")) {
			if (PlayerPrefs.HasKey("faseSalva")) { 
				
				Application.LoadLevel(PlayerPrefs.GetInt("faseSalva") + 2);  
				
			}
			else{
				Application.LoadLevel("Menu");
			}
		}


	}
}
