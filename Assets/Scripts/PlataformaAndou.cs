﻿using UnityEngine;
using System.Collections;

public class PlataformaAndou : MonoBehaviour {

	// Use this for initialization
	private bool pisou;
	public float velocidade;
	public float duracaoPosicao;
	public float tempo;
	public bool posicao;
	
	void OnCollisionEnter2D(Collision2D colisor) {
		if (colisor.gameObject.name == "Player") {
			pisou = true;
		}
		
		
	}
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (pisou) {
			tempo += Time.deltaTime;
			
			//Se o tempo 
			if (tempo >= duracaoPosicao) {
				//Zera contagem
				tempo = 0;
				
				//Muda a posiçao
				if (posicao) {
					posicao = false;
				} else {
					posicao = true;
				}
			}
			//movimenta
			if (posicao) {
				transform.Translate (Vector2.right * velocidade * Time.deltaTime);
			} else {
				transform.Translate (-Vector2.right * velocidade * Time.deltaTime);
			}	
		}
	
	}
	void OnCollisionStay2D(Collision2D colisor) {
		if (colisor.gameObject.name == "Player") {
			colisor.gameObject.transform.parent = transform;
		}
	}
	void OnCollisionExit2D(Collision2D colisor) {
		if (colisor.gameObject.name == "Player") {
			colisor.gameObject.transform.parent = null;
		}
	}
}
